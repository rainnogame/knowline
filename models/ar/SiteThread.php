<?php

namespace app\models\ar;

use Yii;

/**
 * @property string title
 * @property integer id
 * @property integer articlesCount
 * @property integer categoriesCount
 */
class SiteThread extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'thread';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'string', 'max' => 50],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'    => 'ID',
			'title' => 'Название',
		];
	}

	public function getArticlesCount()
	{
		return Article::find()->join('join', 'category', 'category.thread_id = ' . $this->id . ' AND article.category_id = category.id')
			->count();
	}
	public function getCategoriesCount()
	{
		return Category::find()->thread($this->id)
			->count();
	}

}
