<?php

namespace app\models\ar;


use yii\db\ActiveQuery;

/**
 * Class ArticleQuery
 * @package app\models
 * @see Article
 */
class ArticleQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Article[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Article|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function category($id)
    {
        return $this->andWhere(['category_id' => $id]);
    }

    public function tag($tag_id)
    {
        return $this->join('join', 'article_has_tag', "article.id = article_has_tag.article_id AND article_has_tag.tag_id = $tag_id");
    }

    public function type($type)
    {
        return $this->andWhere(['type_id' => $type]);
    }

    public function findMain($threadId)
    {
        return $this->thread($threadId)->andWhere(['priority' => '3']);
    }

    public function thread($threadId)
    {
        return $this->join('join', 'category', 'category.id = article.category_id')->andWhere('category.thread_id = ' . $threadId);
    }

    public function findTop($threadId, $limit = 7)
    {
        return $this->thread($threadId)->andWhere(['!=', 'priority', '3'])->limit($limit);
    }

    public function hasImages()
    {
        return $this->where(['like', 'content', '![](']);
    }

    public function hasTagId($tag_id)
    {
        return $this->join('join', 'article_has_tag', 'article.id = article_has_tag.article_id')->where(['article_has_tag.tag_id' => $tag_id]);
    }


}