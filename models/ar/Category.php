<?php

namespace app\models\ar;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $title
 * @property  integer thread_id
 */
class Category extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'category';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'string', 'max' => 50],
			[['thread_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'    => 'ID',
			'title' => 'Название категории',
		];
	}

	public function beforeSave($insert)
	{
		$this->thread_id = Yii::$app->view->params['thread_id'];

		return parent::beforeSave($insert);
	}

	/**
	 * @return CategoryQuery
	 */
	public static function find()
	{
		return (new CategoryQuery(get_called_class()));
	}

	public function getArticlesCount()
	{
		return Article::find()->category($this->id)->count();
	}

}
