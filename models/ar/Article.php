<?php

namespace app\models\ar;

use voskobovich\linker\LinkerBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string content
 * @property string title
 * @property integer category_id
 * @property string description
 * @property Tag[] tags
 * @property array tag_ids
 * @property Type type
 * @property Category category
 * @property integer views_count
 * @property integer priority
 * @property integer articlesCount
 * @property integer type_id
 */
class Article extends ActiveRecord
{


    public function behaviors()
    {
        return [
            [
                'class'     => LinkerBehavior::className(),
                'relations' => [
                    'tag_ids' => 'tags',
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'string'],
            ['type_id', 'integer'],
            ['description', 'string', 'max' => 500],
            ['content', 'string'],
            ['views_count', 'integer'],
            ['priority', 'integer'],
            ['priority', 'default', 'value' => 2],
            [['tag_ids'], 'each', 'rule' => ['string']],
            ['category_id', 'integer'],
        ];
    }

    public static function tableName()
    {
        return 'article';
    }

    public function beforeSave($insert)
    {
        $tagIds = $this->tag_ids;
        if ($tagIds) {
            foreach ($tagIds as $index => $tagString) {
                if (!is_numeric($tagString)) {
                    $tag = new Tag();
                    $tag->title = $tagString;
                    $tag->save();
                    $tagIds[$index] = $tag->id;
                }
            }
            $this->tag_ids = $tagIds;
        }


        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     * @return ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return (new ArticleQuery(get_called_class()))->orderBy('priority DESC, views_count DESC');
    }

    public function attributeLabels()
    {
        return [
            'category_id' => 'Категория',
            'description' => 'Описание',
            'title'       => 'Заголовок',
            'tag_ids'     => 'Теги',
            'type_id'     => 'Тип статьи',
            'priority'    => 'Приоритет',
            'content'     => 'Текст статьи',
        ];
    }

    public function getType()
    {
        if ($this->type_id) {
            return $this->hasOne(Type::className(), ['id' => 'type_id']);
        } else {
            return new TypeDefault();
        }
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_has_tag', ['article_id' => 'id']);
    }

    public function getCategory()
    {
        if ($this->category_id) {
            return $this->hasOne(Category::className(), ['id' => 'category_id']);
        } else {
            return new CategoryDefault();
        }
    }

    public function getPriorityColor()
    {
        if ($this->priority == 1) {
            return '#CECED4';
        }
        if ($this->priority == 2) {
            return '#9286FF';
        }
        if ($this->priority == 3) {
            return '#FF6B6F';
        }

        return '#fff';
    }

    public function increaseViewsCount()
    {
        $this->views_count++;
    }
}