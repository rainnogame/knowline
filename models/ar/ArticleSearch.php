<?

namespace app\models\ar;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ArticleSearch represents the model behind the search form about `app\models\ar\Article`.
 */
class ArticleSearch extends Article
{
    public $tag_id;
    public $thread_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'type_id', 'views_count', 'priority'], 'integer'],
            [['title', 'content', 'description'], 'safe'],
            ['tag_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();
        if ($this->thread_id) {
            $query->thread($this->thread_id);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'          => $this->id,
            'category_id' => $this->category_id,
            'type_id'     => $this->type_id,
            'views_count' => $this->views_count,
            'priority'    => $this->priority,
        ]);

        if ($this->tag_id) {
            $query->hasTagId($this->tag_id);
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}