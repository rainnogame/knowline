<?php

namespace app\models\ar;


use yii\db\ActiveQuery;

/**
 * Class ArticleQuery
 * @package app\models
 * @see Article
 */
class CategoryQuery extends ActiveQuery
{
	/**
	 * @inheritdoc
	 * @return Article[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * @inheritdoc
	 * @return Article|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}

	/**
	 * @param $id
	 * @return $this
	 */
	public function thread($id)
	{
		return $this->andWhere(['thread_id' => $id]);
	}
}