<?php


namespace app\models\ar;


use Yii;
use yii\db\ActiveRecord;

/**
 * @property string title
 * @property integer id
 * @property integer articlesCount
 * @property string icon
 */
class Type extends ActiveRecord
{
	public function rules()
	{
		return [
			['title', 'string', 'max' => 30,],
			['icon', 'string', 'max' => 25,],
		];
	}

	public function attributeLabels()
	{
		return [
			'title' => 'Название',
			'icon' => 'Иконка FA',
		];
	}

	public function getArticlesCount($onlyInThreads = false)
	{
		$query = Article::find()->where(['type_id' => $this->id]);
		if ($onlyInThreads) {
			$threadId = Yii::$app->view->params['thread_id'];
			$query->join('join','category','category.id = article.category_id');
			$query->andWhere(['category.thread_id' => $threadId]);
		}

		return $query->count();
	}


}