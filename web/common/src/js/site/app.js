import Cookies from '../../libs/js.cookie';
import '../common/jqueryFns';

$(function () {
    $('.toggle-sidebar-btn').on('click', function (e) {
        e.preventDefault();
        if ($(this).data('toggle') === 'categories') {
            $('.articles-meta').slideToggle();
        } else {
            let sidebarOpened = !!$('body').is('.sidebar-collapse');
            Cookies.set("sidebarOpened", sidebarOpened);
        }

    });

    $('body').on('click', function (e) {
        let headerbarOpened = $('.articles-meta').css('display') !== 'none';
        if (headerbarOpened) {
            let $target = $(e.target);
            let canHideHeaderbar = !($target.parents('.articles-meta').length || $target.is('.articles-meta'))
                && !($target.parents('.toggle-sidebar-btn').length || $target.is('.toggle-sidebar-btn'));

            if (canHideHeaderbar) {
                $('.articles-meta').slideToggle();
            }
        }
    });

    $('.js-ask-twice').askTwice();

    $('.sidebar-menu > li > a').hover(function () {
        $(this).trigger('click');
    }, function () {
        return false;
    });


    let $showLinks = $('.js-show-all');
    $showLinks.on('click', function (e) {
        e.preventDefault();
        let self = $(this);
        let articleId = self.data('id');

        let state = self.data('state');
        console.log(state);


        if (state === 'show') {
            hideAllShowed();

            $.post('/article/get-content', {id: articleId}, function (content) {
                self.parent().find('#editormd-view-' + articleId).find('textarea').text(content);
                editormd.markdownToHTML("editormd-view-" + articleId, {
                    htmlDecode: true,
                    tocm: true,
                    tocContainer: ".custom-toc-container",
                    markdownSourceCode: true,
                    emoji: true,
                    taskList: true,
                    tex: true,
                    flowChart: true,
                    sequenceDiagram: true
                });
                self.data('state', 'hide');

                fixLinksStates();
            });
        } else {
            hideAllShowed();
            fixLinksStates();
        }

        function hideAllShowed() {
            $('.js-editormd-view-container').html('<textarea style="display: none"></textarea>');
            $('.js-show-all').data('state', 'show');
        }

        function fixLinksStates() {
            $showLinks.each(function () {
                let state = $(this).data('state');
                if (state === 'show') {
                    $(this).text('Показать всю');
                } else {
                    $(this).text('Скрыть');
                }
            });
        }


    });
});
