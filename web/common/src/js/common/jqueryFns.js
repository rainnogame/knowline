$.fn.askTwice = function () {
    $(this).on('click', function () {
        let askQuest = $(this).data('ask-question');
        let quest = askQuest ? askQuest : 'Вы уверены, что хотете сделать это?';
        return window.confirm(quest);
    });
};
