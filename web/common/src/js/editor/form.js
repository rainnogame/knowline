$(function () {
    if ($('#js-article-editor').length) {

        let articleEditor;

        let articleId = $('#js-article-editor').data('id');
        let editorConfig = {
            width: "100%",
            height: 640,
            syncScrolling: "single",
            taskList: true,
            path: "../lib/",
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL: "/upload/upload-image",

            toolbarIcons: function () {
                return editormd.toolbarModes['full'].concat(['saveArticleContentIcon']);
            },

            toolbarIconsClass: {
                saveArticleContentIcon: 'fa-save'
            },
            onload: function () {
                var keyMap = {
                    'Ctrl-D': function (cm) {

                        var selection = cm.getSelection();
                        var oldCursor = cm.getCursor(false);
                        if (cm.somethingSelected()) {
                            cm.replaceSelection(selection + selection);
                            var newCursor = cm.getCursor(false);
                            cm.setSelection({line: oldCursor.line, ch: oldCursor.ch}, {
                                line: newCursor.line,
                                ch: newCursor.ch
                            });
                        } else {
                            var lineContent = cm.doc.getLine(oldCursor.line);
                            cm.replaceRange(lineContent + '\n', {line: oldCursor.line + 1, ch: 0}, {
                                line: oldCursor.line + 1,
                                ch: 0
                            });
                            cm.setCursor(oldCursor.line + 1, lineContent.length);
                        }
                    },
                    'Ctrl-P': function (cm) {
                        var selection = cm.getSelection();
                        var oldLine = cm.getCursor(false).line;
                        cm.replaceSelection("```\n" + selection + "\n```");
                        cm.setCursor(oldLine, 4);
                    },
                    'Shift-Ctrl-Enter': function (cm) {
                        var oldLine = cm.getCursor(false).line;
                        cm.replaceRange('\n', {line: oldLine + 1, ch: 0}, {line: oldLine + 1, ch: 0});
                        cm.setCursor(oldLine + 1, 0);
                    },
                    'Shift-Ctrl-U': function (cm) {
                        var oldCursor = cm.getCursor(false);
                        var lineContent = cm.doc.getLine(oldCursor.line);

                        $.post('/upload/upload-image-by-url', {
                            url: lineContent
                        }, function (resultUrl) {
                            cm.replaceRange('![](' + resultUrl + ')', {line: oldCursor.line, ch: 0}, {
                                line: oldCursor.line,
                                ch: lineContent.length
                            });
                        });

                    },
                    'Ctrl-Alt-T': function (cm) {
                        console.log('ct-al-t');
                        var oldCursor = cm.getCursor(false);
                        var lineContent = cm.doc.getLine(oldCursor.line);

                        cm.replaceRange('- [ ] ' + lineContent, {line: oldCursor.line, ch: 0}, {
                            line: oldCursor.line,
                            ch: lineContent.length
                        });

                    },
                    'Ctrl-Alt-L': function (cm) {
                        console.log('ct-al-t');
                        var oldCursor = cm.getCursor(false);
                        var lineContent = cm.doc.getLine(oldCursor.line);

                        cm.replaceRange('- ' + lineContent, {line: oldCursor.line, ch: 0}, {
                            line: oldCursor.line,
                            ch: lineContent.length
                        });

                    },
                    'Ctrl-S': function (cm) {
                        saveArticleText();
                    },
                    'Ctrl-Q': function (cm) {
                        var cursor = cm.getCursor();
                        var selection = cm.getSelection();

                        if (cursor.ch !== 0) {
                            cm.setCursor(cursor.line, 0);
                            cm.replaceSelection("> " + selection);
                            cm.setCursor(cursor.line, cursor.ch + 2);
                        } else {
                            cm.replaceSelection("> " + selection);
                        }
                    }
                };
                this.addKeyMap(keyMap);
            },
            toolbarHandlers: {
                saveArticleContentIcon: function (cm, icon, cursor, selection) {
                    saveArticleText();
                }
            }

        };

        articleEditor = editormd("js-article-editor", editorConfig);

        function saveArticleText() {
            $.post('/article/save-content-ajax', {
                id: articleId,
                content: articleEditor.getMarkdown()
            });
        }
    }
});