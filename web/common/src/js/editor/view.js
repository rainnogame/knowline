$(function () {
    if($('#js-article-view').length) {
        let testEditormdView = editormd.markdownToHTML("js-article-view", {
            // markdown: $('#append-test').text(),//+ "\r\n" + $("#append-test").text(),
            htmlDecode: true,       // 开启 HTML 标签解析，为了安全性，默认不开启
            // htmlDecode: "style,script,iframe",  // you can filter tags decode
//            toc: true,
            tocm: true,    // Using [TOCM]
            tocContainer: ".js-article-view__toc", // 自定义 ToC 容器层
            //gfm             : false,
//            tocDropdown: true,
            markdownSourceCode: true,
            emoji: true,
            taskList: true,
            tex: true,
            flowChart: true,
            sequenceDiagram: true
        });
    }
});