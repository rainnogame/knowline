<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require('/var/www/html/helpers/DockerEnv.php');

\DockerEnv::init();

$config = \DockerEnv::webConfig();

$application = new yii\web\Application($config);
$application->run();

