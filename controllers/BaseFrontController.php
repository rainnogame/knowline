<?php


namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;


/**
 *
 * @property string|int $threadId
 */
abstract class BaseFrontController extends Controller
{
	const DEFAULT_THREAD_ID = 1;
	const DENIED_THREAD = 4;

	public function isPost()
	{
		return \Yii::$app->request->isPost;
	}

	public function isGet()
	{
		return \Yii::$app->request->isGet;
	}

	public function isAjax()
	{
		return \Yii::$app->request->isAjax;
	}

	public function post($param = null, $default = null)
	{
		return \Yii::$app->request->post($param, $default);
	}

	public function get($param = null, $default = null)
	{
		return \Yii::$app->request->post($param, $default);
	}

	public function init()
	{
		$this->initThread();
		parent::init();
	}

	public function initThread()
	{
		$threadId = $this->getThreadId();
		$this->view->params['thread_id'] = $threadId;
	}

	/**
	 * @return int|string
	 */
	public function getThreadId()
	{
		$threadCookie = Yii::$app->request->cookies->get('thread_id');
		$threadId = $threadCookie->value ?? self::DEFAULT_THREAD_ID;

		return $threadId;
	}

	public function beforeAction($action)
	{
		\Yii::$app->user->setReturnUrl(\Yii::$app->request->referrer);

		return parent::beforeAction($action);
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [

					[
						'allow'         => true,
						'matchCallback' => function ($rule, $action) {
							$threadId = Yii::$app->view->params['thread_id'];
							$allow = ($threadId == self::DENIED_THREAD) ? !\Yii::$app->user->isGuest : true;

							return $allow;
						},
					],
					[
						'allow'   => true,
						'actions' => ['login', 'signup'],
						'roles'   => ['?'],
					],
					[
						'allow'   => true,
						'actions' => ['logout'],
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

}