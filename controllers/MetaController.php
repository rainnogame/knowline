<?php


namespace app\controllers;


use app\models\ar\Category;
use app\models\ar\SiteThread;
use app\models\ar\Tag;
use app\models\ar\Type;

class MetaController extends BaseFrontController
{

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionCreateTag()
	{
		$tag = new Tag();
		if ($tag->load($this->post()) && $tag->save()) {
			return $this->goBack();
		}

		return $this->render('tag-form', ['tag' => $tag]);
	}

	public function actionEditTag($id)
	{
		$tag = Tag::findOne($id);
		if ($tag->load($this->post()) && $tag->save()) {
			return $this->goBack();
		}

		return $this->render('tag-form', ['tag' => $tag]);
	}

	public function actionCreateCategory()
	{
		$category = new Category();
		if ($category->load($this->post()) && $category->save()) {
			return $this->goBack();
		}

		return $this->render('category-form', ['category' => $category]);
	}

	public function actionEditCategory($id)
	{
		$category = Category::findOne($id);
		if ($category->load($this->post()) && $category->save()) {
			return $this->goBack();
		}

		return $this->render('category-form', ['category' => $category]);
	}

	public function actionCreateType()
	{
		$category = new Type();
		if ($category->load($this->post()) && $category->save()) {
			return $this->goBack();
		}

		return $this->render('type-form', ['type' => $category]);
	}

	public function actionEditType($id)
	{
		$category = Type::findOne($id);
		if ($category->load($this->post()) && $category->save()) {
			return $this->redirect('/');
		}

		return $this->render('type-form', ['type' => $category]);
	}

	public function actionCreateThread()
	{
		$category = new SiteThread();
		if ($category->load($this->post()) && $category->save()) {
			return $this->goBack();
		}

		return $this->render('thread-form', ['thread' => $category]);
	}

	public function actionEditThread($id)
	{
		$category = SiteThread::findOne($id);
		if ($category->load($this->post()) && $category->save()) {
			return $this->goBack();
		}

		return $this->render('thread-form', ['thread' => $category]);
	}

	public function actionDeleteTag($id)
	{
		Tag::deleteAll(['id' => $id]);

		return $this->goBack();
	}

	public function actionDeleteCategory($id)
	{
		Category::deleteAll(['id' => $id]);

		return $this->goBack();
	}

	public function actionDeleteThread($id)
	{
		/** @var SiteThread $thread */
		$thread = SiteThread::findOne($id);
		if ($thread->categoriesCount == 0) {
			SiteThread::deleteAll(['id' => $id]);
		}


		return $this->goBack();
	}

	public function actionDeleteType($id)
	{
		Type::deleteAll(['id' => $id]);

		return $this->goBack();
	}

}