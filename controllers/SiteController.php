<?php

namespace app\controllers;

use app\models\ar\Article;
use app\models\ar\ArticleSearch;
use app\models\ar\User;
use app\models\forms\{
    LoginForm, PasswordResetRequestForm, ResetPasswordForm
};
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Cookie;

class SiteController extends BaseFrontController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionChangeThread($thread_id)
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name'  => 'thread_id',
            'value' => $thread_id,
        ]));

        return $this->redirect('/');
    }

    /**
     * Render the homepage
     */
    public function actionIndex2()
    {
        $threadId = Yii::$app->view->params['thread_id'];
        $mainArticles = Article::find()->findMain($threadId)->all();
        $topArticles = Article::find()->findTop($threadId)->all();

        return $this->render('index2', ['mainArticles' => $mainArticles, 'topArticles' => $topArticles]);
    }

    /**
     * Render the homepage
     */
    public function actionIndex()
    {
        $threadId = Yii::$app->view->params['thread_id'];
        $searchModel = new ArticleSearch([
            'thread_id' => $threadId,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionList($cat_id = null, $tag_id = null, $type_id = null)
    {
        $threadId = Yii::$app->view->params['thread_id'];

        $searchModel = new ArticleSearch([
            'thread_id'   => $threadId,
            'category_id' => $cat_id,
            'tag_id'      => $tag_id,
            'type_id'     => $type_id,

        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }


    /**
     * User login
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * User logout
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * User signup
     */
    public function actionSignup()
    {
        $user = new User(['scenario' => 'signup']);
        if ($user->load(Yii::$app->request->post()) && $user->save()) {
            Yii::$app->session->setFlash('user-signed-up');

            return $this->refresh();
        }

        if (Yii::$app->session->hasFlash('user-signed-up')) {
            return $this->render('signedUp');
        }

        return $this->render('signup', [
            'model' => $user,
        ]);
    }

    /**
     * Confirm email
     */
    public function actionConfirmEmail($token)
    {
        $user = User::find()->emailConfirmationToken($token)->one();

        if ($user !== null && $user->confirmEmail()) {
            Yii::$app->getUser()->login($user);

            return $this->goHome();
        }

        return $this->render('emailConfirmationFailed');
    }

    /**
     * Request password reset
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Reset password
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
