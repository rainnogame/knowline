<?php


namespace app\controllers;


use yii\web\Controller;
use yii\web\UploadedFile;

class UploadController extends BaseFrontController
{
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;

		return true;
	}

	public function actionUploadImage()
	{
		/** @var UploadedFile $uploadedFile */
		$uploadedFile = UploadedFile::getInstancesByName('editormd-image-file')[0];
		$fileName = $this->buildImageName($uploadedFile->extension);
		$uploadedFile->saveAs($this->buildImagePath($fileName));

		return json_encode([
			'success' => 1,
			'message' => 'Upload success',
			'url'     => '/uploads/' . $fileName,
		]);
	}

	/**
	 * @param $extension
	 * @return string
	 */
	public function buildImageName($extension): string
	{
		return uniqid() . '.' . $extension;
	}

	/**
	 * @param $fileName
	 * @return string
	 */
	public function buildImagePath($fileName): string
	{
		return \Yii::getAlias('@webroot') . '/uploads/' . $fileName;
	}

	public function actionUploadImageByUrl()
	{
		$url = $this->post('url');
		if ($url) {
			$ext = $this->getExtFromUrl($url);
			if ($this->isValidExt($ext)) {
				$fileName = uniqid() . '.' . $ext;
				$imgPath = $this->buildImagePath($fileName);
				file_put_contents($imgPath, file_get_contents($url));
				return '/uploads/' . $fileName;
			}
		}

		return false;

	}

	/**
	 * @param $ext
	 * @return bool
	 */
	public function isValidExt($ext): bool
	{
		return $ext && in_array($ext, ['png', 'jpg', 'gif', 'jpeg']);
	}

	/**
	 * @param $url
	 * @return mixed
	 */
	public function getExtFromUrl($url)
	{
		$urlParts = explode('.', $url);
		$ext = $urlParts[count($urlParts) - 1];

		return $ext;
	}


}