<?php


namespace app\controllers;


use app\helpers\Request;
use app\models\ar\Article;
use app\models\ar\ArticleQuery;
use app\models\ar\Category;
use app\models\ar\Tag;
use app\models\ar\Type;
use Yii;
use yii\web\Controller;

class ArticleController extends BaseFrontController
{
    public function actionView($id)
    {
        /** @var Article $article */
        $article = Article::findOne($id);
        $article->increaseViewsCount();
        $article->save();

        return $this->render('view', ['article' => $article]);
    }

    public function actionCreate()
    {
        $article = new Article();
        if ($article->load($this->post()) && $article->save()) {
            return $this->redirect(['view', 'id' => $article->id]);
        }

        return $this->render('form', ['article' => $article]);
    }

    public function actionClearImages()
    {
        $articlesWithImages = Article::find()->hasImages()->all();

        $existencesImages = $this->getExistencesImages($articlesWithImages);

        $uploadedImages = $this->getUploadedImages();

        $this->deleteNonExitsImages($uploadedImages, $existencesImages);

        return $this->goBack();
    }

    /**
     * @param $articlesWithImages
     * @return array
     */
    public function getExistencesImages($articlesWithImages): array
    {
        $allowedImages = [];
        foreach ($articlesWithImages as $article) {
            $matches = [];
            preg_match_all('/!\[[^\[\]]*\]\(\/uploads\/([^\(\)]*\.(png|gif|jpg|jpeg))[^\(\)]*\)/ui', $article->content, $matches);
            if (count($matches) == 3 && isset($matches[1])) {
                $allowedImages = array_merge($allowedImages, $matches[1]);
            }

        }

        return $allowedImages;
    }

    /**
     * @return array
     */
    public function getUploadedImages(): array
    {
        $uploadedImages = scandir(Yii::getAlias('@webroot' . '/uploads/'));
        // remove . and .. from array
        unset($uploadedImages[0]);
        unset($uploadedImages[1]);

        return $uploadedImages;
    }

    /**
     * @param $uploadedImages
     * @param $existencesImages
     */
    public function deleteNonExitsImages($uploadedImages, $existencesImages)
    {
        $uploadsPath = Yii::getAlias('@webroot' . '/uploads/');
        foreach ($uploadedImages as $img) {
            if (!in_array($img, $existencesImages)) {
                unlink($uploadsPath . $img);
            }
        }
    }

    public function actionGetContent()
    {
        $articleId = $this->post('id');
        $content = Article::findOne($articleId)->content;

        return $content;
    }

    public function actionEdit($id)
    {
        /** @var Article $article */
        $article = Article::findOne($id);

        if ($article->load($this->post()) && $article->save()) {
            return $this->redirect(['view', 'id' => $article->id]);
        }

        return $this->render('form', ['article' => $article]);
    }

    public function actionDelete($id)
    {
        Article::deleteAll(['id' => $id]);

        return $this->redirect(['list']);
    }

    public function actionSaveContentAjax()
    {
        if (Request::isAjax()) {
            $id = Request::post('id');
            $article = Article::findOne($id);
            $article->content = Request::post('content');

            return $article->save();
        } else {
            return false;
        }

    }
}