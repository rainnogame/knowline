<?

use yii\web\View;

/** @var \app\models\ar\Article $model */
/** @var View $this */

?>

<?= $this->render('/includes/article/item', [
    'model' => $model,
]); ?>