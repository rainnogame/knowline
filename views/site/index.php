<?php

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ar\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-container">

    <?php Pjax::begin([
        'linkSelector' => '.yii-pjax-link',
    ]); ?>

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView'     => '_article_item',
        'summary'      => false,

    ]); ?>
    <?php Pjax::end(); ?>
</div>