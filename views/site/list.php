<?php

/* @var yii\web\View $this */

use app\assets\EditormdViewAsset;
use app\models\ar\Article;
use app\widgets\articles_list\ArticlesList;

$label = '';
if ($type = \app\helpers\Request::get('type')) {
//	$label = \app\models\enums\ArticleType::getLabel($type);
}

$catId = \app\helpers\Request::get('cat_id');

if ($catId === '') {
	$label = 'Все категории';
} elseif (is_numeric($catId)) {
	$label = \app\models\ar\Category::findOne($catId)->title;
}

if ($tagId = \app\helpers\Request::get('tag_id')) {
	$label = \app\models\ar\Tag::findOne($tagId)->title;
}


$this->params['breadcrumbs'][] = [
	'label' => $label,
]

?>
<? /** @var Article[] $articles */

EditormdViewAsset::register($this);
?>
<?= ArticlesList::widget(['articles' => $articles]) ?>
