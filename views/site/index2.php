<?php
use app\widgets\articles_list\ArticlesList;

/* @var yii\web\View $this */
/** @var \app\models\ar\Article[] $mainArticles */
/** @var \app\models\ar\Article[] $topArticles */


?>
<div class="row">
    <div class="col-md-6">
        <h3>Важные</h3>
		<?= ArticlesList::widget(['articles' => $mainArticles]) ?>
    </div>
    <div class="col-md-6">
        <h3>Самые просматриваемые</h3>
		<?= ArticlesList::widget(['articles' => $topArticles]) ?>
    </div>
</div>


