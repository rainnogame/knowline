<?php

use app\helpers\HTML;
use app\models\ar\Type;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ar\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search">


    <?php $form = ActiveForm::begin([
        'method'  => 'get',
        'options' => [
            'data-pjax' => 1,
        ],
    ]); ?>

    <?= $form->field($model, 'tag_id', [
        'options' => [
            'data-type-field' => 'tag',
        ],
    ])->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'category_id', [
        'options' => [
            'data-type-field' => 'category',
        ],
    ])->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'type_id', [
        'options' => [
            'data-type-field' => 'type',
        ],
    ])->hiddenInput()->label(false); ?>
    <div class="row">

        <div class="col-md-3">
            <h3>Теги</h3>
            <?
            $tags = \app\models\ar\Tag::find()->all();
            ?>
            <? foreach ($tags as $tag) : ?>
                <div>
                    <?= HTML::a($tag->title . ' [' . $tag->articlesCount . ']', '#', [
                        'class'     => 'btn btn-xs btn-default filter-btn ' . ($model->tag_id == $tag->id ? 'type-checked' : ''),
                        'data-type' => 'tag',
                        'data-id'   => $tag->id,
                    ]) ?>
                </div>
            <? endforeach; ?>


        </div>

        <div class="col-md-3">

            <h3>Категории</h3>
            <?
            $categories = \app\models\ar\Category::find()->thread(Yii::$app->view->params['thread_id'])->all();
            ?>
            <? foreach ($categories as $category) : ?>
                <div>
                    <?= HTML::a($category->title . ' [' . $category->articlesCount . ']', '#', [
                        'class'     => 'btn btn-xs btn-default filter-btn ' . ($model->category_id == $category->id ? 'type-checked' : ''),
                        'data-type' => 'category',
                        'data-id'   => $category->id,
                    ]) ?>
                </div>
            <? endforeach; ?>


        </div>
        <div class="col-md-3">

            <h3>Типы</h3>
            <?
            /** @var Type[] $types */
            $types = Type::find()->all();
            ?>
            <? foreach ($types as $type) : ?>
                <div>
                    <?= HTML::a($type->title . ' [' . $type->getArticlesCount(true) . ']', '#', [
                        'class'     => 'btn btn-xs btn-default filter-btn ' . ($model->type_id == $type->id ? 'type-checked' : ''),
                        'data-type' => 'type',
                        'data-id'   => $type->id,
                    ]) ?>
                </div>
            <? endforeach; ?>


        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'title') ?>
            <div class="box collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Поиск по содержимому</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title=""
                                data-original-title="Collapse">
                            <i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= $form->field($model, 'content') ?>
                    <?= $form->field($model, 'description') ?>
                </div>
                <!-- /.box-footer-->
            </div>
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <!--                --><? //= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <style>
        .type-checked {
            background-color: #ccc;
        }
    </style>
    <script>
        $('.filter-btn').on('click', function () {
            $(this).toggleClass('type-checked');
            var dataTypeFiled = $(this).data('type');
            var dataId = $(this).data('id');
            if ($(this).is('.type-checked')) {
                $('[data-type-field=' + dataTypeFiled + '] input').val(dataId);
            } else {
                $('[data-type-field=' + dataTypeFiled + '] input').val('');
            }
            $(this).parents('form').trigger('submit');
        });
    </script>
</div>