<!-- todoc temp hiden-->
<aside class="main-sidebar" style="position: fixed; display: none">

    <section class="sidebar">
        <?
        /** @var \app\models\ar\Category[] $categories */
        $categories = \app\models\ar\Category::find()->thread(Yii::$app->view->params['thread_id'])->all();

        $categoriesMenu = [];
        $categoriesMenu[] = ['label' => 'Все категории', 'icon' => 'database  ', 'url' => ['/article/list', 'cat_id' => '']];
        foreach ($categories as $category) {
            $categoriesMenu[] = ['label' => $category->title, 'icon' => 'circle-o', 'url' => ['/article/list', 'cat_id' => $category->id]];
        }

        /** @var \app\models\ar\SiteThread[] $threads */
        $threads = \app\models\ar\SiteThread::find()->all();

        $threadsMenu = [];
        foreach ($threads as $thread) {
            $threadsMenu[] = ['label' => $thread->title, 'icon' => 'circle-o', 'url' => ['/site/change-thread', 'thread_id' => $thread->id]];
        }

        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items'   => [
                    ['label' => 'Треды', 'icon' => 'bars', 'items' => $threadsMenu],
                    ['label' => 'Категории', 'icon' => 'bars', 'items' => $categoriesMenu],
//					['label' => 'Мета информация', 'icon' => 'hashtag', 'url' => ['/meta']],
                ],
            ]
        ) ?>

    </section>

</aside>
