<?php


/* @var $this \yii\web\View */

use app\helpers\HTML;
use app\models\ar\User;

/* @var $content string */
?>
<?
$thread = \app\models\ar\SiteThread::findOne($this->params['thread_id']);
?>

<header class="main-header" style="position: fixed; width: 100%;">
	<?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . $thread->title . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">

        <!-- todoc temp commented -->
        <!-- <a href="#" class="toggle-sidebar-btn" data-toggle="offcanvas">
            <i class="fa fa-bars"></i>
        </a>-->
        <a href="#" class="toggle-sidebar-btn" data-toggle="categories">
            <i class="fa fa-arrow-circle-o-down"></i>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
					<?= HTML::a('<i class="fa fa-file-text-o"></i>', ['/article/create'], []); ?>
                </li>
                <li>
					<?= HTML::a('<i class="fa fa-superpowers"></i>', ['/article/clear-images'], []); ?>
                </li>
                <li class="dropdown user user-menu">
					<? if (Yii::$app->user->isGuest): ?>
                        <a href="<?= \yii\helpers\Url::to(['/site/login']) ?>">
                            <span class="hidden-xs">Авторизация</span>
                        </a>
					<? else: ?>
						<?
						/** @var User $user */
						$user = Yii::$app->user->identity;
						?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image"
                                 alt="User Image"/>
                            <span class="hidden-xs"><?= $user->username ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                                     alt="User Image"/>

                                <p>
	                                <?= $user->username ?>
                                    <small>Киев, Украина</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
									<?= Html::a(
										'Выйти',
										['/site/logout'],
										['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
									) ?>
                                </div>
                            </li>
                        </ul>
					<? endif; ?>

                </li>
            </ul>
        </div>
    </nav>
</header>