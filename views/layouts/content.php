<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>

<div class="content-wrapper" style="margin-top: 50px;">

	<?= Breadcrumbs::widget(
		[
			'homeLink' => [
				'label' => 'Главная',
				'url'   => '/',
			],
			'links'    => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]
	); ?>
    <section class="content">
	    <?= $this->render('//meta/index') ?>
		<?= Alert::widget() ?>
		<?= $content ?>
    </section>
</div>

<aside class="control-sidebar control-sidebar-light">
</aside>
<div class='control-sidebar-bg'></div>
<footer class="main-footer">

</footer>
