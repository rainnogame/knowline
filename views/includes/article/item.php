<?

use app\helpers\HTML;
use yii\web\View;

/** @var View $this */
/** @var \app\models\ar\Article $model */
?>


<div class="article-view">
    <hr>
    <div style="background-color: <?= $model->getPriorityColor() ?>; height: 5px; width: 20px"></div>
    <?
    $tagsButtons = '';
    foreach ($model->tags as $tag) {
        $tagsButtons .= HTML::a('#' . $tag->title, ['/article/list', 'tag_id' => $tag->id]);
    }
    ?>
    <div class="article-view__header">

        <div class="pull-left">
            <?= HTML::typeIcon($model->type->icon); ?>
            <?= HTML::a($model->title, ['/article/view', 'id' => $model->id]); ?>
            <?= HTML::a('[' . $model->type->title . ']', ['/article/list', 'type' => $model->type], [
                'class' => 'article-type-title',
            ]); ?>
            <span class="article-view__views-count"><?= $model->views_count; ?></span>
        </div>
        <div class="pull-right">
            <?= HTML::a($model->category->title, ['/article/list', 'cat_id' => $model->category_id], [
                'class' => 'btn btn-xs btn-default',
            ]) ?>
            <?= HTML::a('<i class="fa fa-pencil"></i>', ['/article/edit', 'id' => $model->id]) ?>
            <?= HTML::a('<i class="fa fa-times"></i>', ['/article/delete', 'id' => $model->id], [
                'class' => 'js-ask-twice',
            ]); ?>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="article-view__description">
        <?= $model->description??'Нет описания'; ?>
    </div>
    <div>
        <?= $tagsButtons ?>
    </div>
    <div>
        <a data-state="show" href="#" data-id="<?= $model->id ?>" class="js-show-all">Показать всю</a>
        <div class="js-editormd-view-container" id="editormd-view-<?= $model->id ?>">
            <textarea style="display: none"></textarea>
        </div>
    </div>
</div>