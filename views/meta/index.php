<?

use app\helpers\HTML;
use app\helpers\PHP;
use app\models\ar\SiteThread;
use app\models\ar\Type;
use yii\web\View;

/** @var View $this */

$this->title = 'Мета информация';
//$this->params['breadcrumbs'][] = [
//	'label' => $this->title,
//];
?>
<?
$headerbarOpened = PHP::isDefArr($_COOKIE, 'headerbarOpened', true);
?>
<div class="row articles-meta">
    <div class="col-md-3">
        <h3>Теги</h3>
        <?
        $tags = \app\models\ar\Tag::find()->all();
        ?>
        <?= HTML::a('Создать тег', ['/meta/create-tag'], ['class' => 'btn btn-xs btn-success']); ?>
        <hr>
        <? foreach ($tags as $tag) : ?>
            <div>
                <?= HTML::a($tag->title . ' [' . $tag->articlesCount . ']', ['/site/list', 'tag_id' => $tag->id], ['class' => 'btn btn-xs btn-default']) ?>
                <?= HTML::a('<i class="fa fa-pencil"></i>',
                    ['/meta/edit-tag', 'id' => $tag->id], ['class' => 'btn btn-xs btn-default']) ?>
                <?= HTML::a('<i class="fa fa-times"></i>',
                    ['/meta/delete-tag', 'id' => $tag->id], ['class' => 'btn btn-xs btn-default js-ask-twice']) ?>
            </div>
        <? endforeach; ?>


    </div>

    <div class="col-md-3">

        <h3>Категории</h3>
        <?
        $categories = \app\models\ar\Category::find()->thread(Yii::$app->view->params['thread_id'])->all();
        ?>
        <?= HTML::a('Создать категорию', ['/meta/create-category'], ['class' => 'btn btn-xs btn-success']); ?>
        <hr>
        <? foreach ($categories as $category) : ?>
            <div>
                <?= HTML::a($category->title . ' [' . $category->articlesCount . ']', ['/site/list', 'cat_id' => $category->id], ['class' => 'btn btn-xs btn-default']) ?>
                <?= HTML::a('<i class="fa fa-pencil"></i>',
                    ['/meta/edit-category', 'id' => $category->id], ['class' => 'btn btn-xs btn-default']) ?>
                <? if ($category->articlesCount == 0): ?>
                    <?= HTML::a('<i class="fa fa-times"></i>',
                        ['/meta/delete-category', 'id' => $category->id], ['class' => 'btn btn-xs btn-default js-ask-twice']) ?>
                <? endif; ?>
            </div>
        <? endforeach; ?>


    </div>
    <div class="col-md-3">

        <h3>Типы</h3>
        <?
        /** @var Type[] $types */
        $types = Type::find()->all();
        ?>
        <?= HTML::a('Создать тип', ['/meta/create-type'], ['class' => 'btn btn-xs btn-success']); ?>
        <hr>
        <? foreach ($types as $type) : ?>
            <div>
                <?= HTML::a($type->title . ' [' . $type->getArticlesCount(true) . ']',
                    ['/site/list', 'type_id' => $type->id], ['class' => 'btn btn-xs btn-default']) ?>
                <?= HTML::a('<i class="fa fa-pencil"></i>',
                    ['/meta/edit-type', 'id' => $type->id], ['class' => 'btn btn-xs btn-default']) ?>
                <?= HTML::a('<i class="fa fa-times"></i>',
                    ['/meta/delete-type', 'id' => $type->id], ['class' => 'btn btn-xs btn-default js-ask-twice']); ?>
            </div>
        <? endforeach; ?>


    </div>
    <div class="col-md-3">

        <h3>Треды</h3>
        <?
        /** @var SiteThread[] $threads */
        $threads = SiteThread::find()->all();
        ?>
        <?= HTML::a('Создать тред', ['/meta/create-thread'], ['class' => 'btn btn-xs btn-success']); ?>
        <hr>
        <? foreach ($threads as $thread) : ?>
            <div>
                <?= HTML::a($thread->title . ' [' . $thread->articlesCount . ']' . ' [' . $thread->categoriesCount . ']',
                    ['/site/change-thread', 'thread_id' => $thread->id], ['class' => 'btn btn-xs btn-default']) ?>
                <?= HTML::a('<i class="fa fa-pencil"></i>',
                    ['/meta/edit-thread', 'id' => $thread->id], ['class' => 'btn btn-xs btn-default']) ?>
                <? if ($thread->categoriesCount == 0): ?>
                    <?= HTML::a('<i class="fa fa-times"></i>', ['/meta/delete-thread', 'id' => $thread->id], [
                        'class'             => 'btn btn-xs btn-default js-ask-twice',
                        'data-ask-question' => 'Вы уверены? Все категории и задачи к ним будут перенесены в тред по умолчанию!',
                    ]); ?>
                <? endif; ?>
            </div>
        <? endforeach; ?>

    </div>
</div>