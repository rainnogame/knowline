<?
use app\helpers\HTML;
use kartik\form\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var \app\models\ar\Tag $tag */

$this->title = $tag->title;
?>

<? $form = ActiveForm::begin(); ?>


<?= $form->field($tag, 'title'); ?>

<?= HTML::submitButton('Сохранить', [
	'class' => 'btn btn-xs btn-default',
]) ?>

<? ActiveForm::end(); ?>