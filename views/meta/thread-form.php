<?
use app\helpers\HTML;
use kartik\form\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var \app\models\ar\SiteThread $thread */

$this->title = $thread->title;
?>

<? $form = ActiveForm::begin(); ?>


<?= $form->field($thread, 'title'); ?>

<?= HTML::submitButton('Сохранить', [
	'class' => 'btn btn-xs btn-default',
]) ?>

<? ActiveForm::end(); ?>