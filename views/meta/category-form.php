<?
use app\helpers\HTML;
use kartik\form\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var \app\models\ar\Category $category */

$this->title = $category->title;
?>

<? $form = ActiveForm::begin(); ?>


<?= $form->field($category, 'title'); ?>

<?= HTML::submitButton('Сохранить', [
	'class' => 'btn btn-xs btn-default',
]) ?>

<? ActiveForm::end(); ?>