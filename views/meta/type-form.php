<?
use app\helpers\HTML;
use kartik\form\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var \app\models\ar\Type $type */

$this->title = $type->title;
?>

<? $form = ActiveForm::begin(); ?>


<?= $form->field($type, 'title'); ?>
<?= $form->field($type, 'icon'); ?>

<?= HTML::submitButton('Сохранить', [
	'class' => 'btn btn-xs btn-default',
]) ?>
    <div>
        <a href="http://fontawesome.io/icons/">http://fontawesome.io/icons/</a>
    </div>

<? ActiveForm::end(); ?>