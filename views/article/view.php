<?

use app\assets\EditormdViewAsset;
use app\helpers\HTML;
use app\models\ar\Article;
//use app\models\enums\ArticleType;
use app\widgets\Box;
use yii\web\View;

/** @var View $this */

/** @var Article $article */
$this->title = $article->title;
EditormdViewAsset::register($this);

$category = $article->category;

$this->params['breadcrumbs'][] = [
	'label' => $category->title,
	'url'   => ['/article/list', 'cat_id' => $category->id],
];
$this->params['breadcrumbs'][] = [
	'label' => $this->title,
]

?>

<div class="article-view">
	<?
	$tagsButtons = '';
	foreach ($article->tags as $tag) {
		$tagsButtons .= HTML::a('#' . $tag->title, ['/article/list', 'tag_id' => $tag->id]);
	}
	?>
    <div class="article-view__header">
        <div class="pull-left">
            <span style="background-color: <?= $article->getPriorityColor() ?>">&nbsp;&nbsp;</span>
			<?= HTML::a($article->category->title . ': ', ['/article/list', 'cat_id' => $article->category_id]); ?>
			<?= HTML::a($article->title, ['/article/view', 'id' => $article->id]); ?>
			<?= HTML::a('[' . $article->type->title . ']', ['/article/list', 'type' => $article->type]); ?>
            <span class="article-view__views-count"><?= $article->views_count; ?></span>
        </div>
        <div class="pull-right">
			<?= HTML::a('<i class="fa fa-pencil"></i>', ['/article/edit', 'id' => $article->id]) ?>
			<?= HTML::a('<i class="fa fa-times"></i>', ['/article/delete', 'id' => $article->id], [
				'class' => 'js-ask-twice',
			]); ?>
        </div>
        <div class="clearfix"></div>
    </div>



    <div>
		<?= $tagsButtons ?>
    </div>
    <h4>Содержание</h4>
    <div class="markdown-body editormd-preview-container js-article-view__toc"></div>
    <div id="js-article-view">
        <textarea style="display:none;"><?= $article->content ?></textarea>
    </div>

    <a class="content-btn" href="#" data-toggle="modal" data-target=".article-view__toc-modal">
        <i class="fa fa-book"></i>
    </a>

    <div class="modal fade article-view__toc-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Содержание</h4>
                </div>
                <div class="modal-body">
                    <div class="markdown-body editormd-preview-container js-article-view__toc"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>

        </div>
    </div>
</div>