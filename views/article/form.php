<?php
/* @var yii\web\View $this */

/** @var Article $article */

use app\helpers\HTML;
use app\models\ar\Article;
use app\models\ar\Category;
use app\models\ar\Tag;
use app\models\ar\Type;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$title = $article->isNewRecord ? 'Новая статья' : $article->title;

$this->title = $title;


$category = $article->category;

$this->params['breadcrumbs'][] = [
	'label' => $category->title,
	'url'   => ['/article/list', 'cat_id' => $category->id],
];
$this->params['breadcrumbs'][] = [
	'label' => $this->title,
	'url'   => ['/article/view', 'id' => $article->id,],
];
$this->params['breadcrumbs'][] = [
	'label' => 'Редактировать',
]


?>


<? $form = ActiveForm::begin(); ?>

<?= HTML::submitButton('Сохранить', [
	'class' => 'btn btn-default',
	'style' => 'width: 100%',
]); ?>
<div class="article-edit-content">

    <div class="row">
        <div class="col-md-5">
			<?= $form->field($article, 'title'); ?>
        </div>
        <div class="col-md-4">
			<?= $form->field($article, 'category_id')->widget(Select2::className(), [
				'data'          => ArrayHelper::map(Category::find()->thread($this->params['thread_id'])->all(), 'id', 'title'),
				'options'       => ['placeholder' => 'Выберите категорию ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>
        </div>
        <div class="col-md-3">
			<?=
			$form->field($article, 'type_id')->widget(Select2::className(), [
				'data'          => ArrayHelper::map(Type::find()->all(), 'id', 'title'),
				'options'       => ['placeholder' => 'Выберите тип ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			])
			?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
			<?=
			$form->field($article, 'tag_ids')->widget(Select2::className(), [
				'data'          => ArrayHelper::map(Tag::find()->all(), 'id', 'title'),
				'options'       => [
					'placeholder' => 'Выберите/создайте теги ...',
					'multiple'    => true,
				],
				'pluginOptions' => [
					'allowClear'      => true,
					'tags'            => true,
					'tokenSeparators' => [','],
				],
			])
			?>
        </div>
        <div class="col-md-4">
			<?= $form->field($article, 'priority')->radioList(
				['1' => 'Не важно', '2' => 'Норм', '3' => 'Важно']
			) ?>
        </div>
    </div>

	<?= $form->field($article, 'description')->textarea([
		'style' => 'height: 150px',
	]); ?>

    <div id="js-article-editor" data-id="<?= $article->id ?>">
		<?= \yii\helpers\Html::textarea('Article[content]', $article->content) ?>
    </div>

</div>

<? ActiveForm::end() ?>
