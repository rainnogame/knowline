<?php


namespace app\assets;


use yii\web\AssetBundle;

class EditormdViewAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $js = [
		'/lib/marked.min.js',
		'/lib/prettify.min.js',
		'/lib/raphael.min.js',
		'/lib/underscore.min.js',
		'/lib/sequence-diagram.min.js',
		'/lib/flowchart.min.js',
		'/lib/jquery.flowchart.min.js',
	];

}