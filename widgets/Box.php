<?php


namespace app\widgets;


use app\helpers\HTML;
use app\helpers\PHP;
use yii\bootstrap\Widget;

class Box extends Widget
{
	public $header;
	public static function begin($config = [])
	{
		ob_start();

		$boxType = PHP::isDefArr($config, 'type', 'primary');
		$header = PHP::isDefArr($config, 'header', []);

		echo HTML::beginTag('div', [
			'class' => "box box-$boxType",
		]);

		if ($header) {
			echo HTML::tag('div', $header, [
				'class' => "box-header",
			]);
		}

		echo HTML::beginTag('div', [
			'class' => "box-body",
		]);
	}

	public static function end($footer = null)
	{
		echo HTML::endTag('div');
		if ($footer) {
			echo HTML::tag('div', $footer, [
				'class' => "box-footer",
			]);
		}
		echo HTML::endTag('div');
		ob_get_flush();
	}


}