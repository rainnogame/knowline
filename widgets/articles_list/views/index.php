<?php

/* @var yii\web\View $this */

use app\assets\EditormdViewAsset;
use app\helpers\HTML;
use app\helpers\PHP;
use app\models\ar\Article;

?>
<? /** @var Article[] $articles */

EditormdViewAsset::register($this);
?>
<div class="articles-container">

	<? foreach ($articles as $index => $article) : ?>

        <div class="article-view">
            <hr>
            <div style="background-color: <?= $article->getPriorityColor() ?>; height: 5px; width: 20px"></div>
			<?
			$tagsButtons = '';
			foreach ($article->tags as $tag) {
				$tagsButtons .= HTML::a('#' . $tag->title, ['/article/list', 'tag_id' => $tag->id]);
			}
			?>
            <div class="article-view__header">

                <div class="pull-left">
					<?= HTML::typeIcon($article->type->icon); ?>
					<?= HTML::a($article->title, ['/article/view', 'id' => $article->id]); ?>
					<?= HTML::a('[' . $article->type->title . ']', ['/article/list', 'type' => $article->type], [
						'class' => 'article-type-title',
					]); ?>
                    <span class="article-view__views-count"><?= $article->views_count; ?></span>
                </div>
                <div class="pull-right">
					<?= HTML::a($article->category->title, ['/article/list', 'cat_id' => $article->category_id], [
						'class' => 'btn btn-xs btn-default',
					]) ?>
					<?= HTML::a('<i class="fa fa-pencil"></i>', ['/article/edit', 'id' => $article->id]) ?>
					<?= HTML::a('<i class="fa fa-times"></i>', ['/article/delete', 'id' => $article->id], [
						'class' => 'js-ask-twice',
					]); ?>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="article-view__description">
				<?= PHP::isDef($article->description, 'Нет описания'); ?>
            </div>
            <div>
				<?= $tagsButtons ?>
            </div>
            <div>
                <a data-state="show" href="#" data-id="<?= $article->id ?>" class="js-show-all">Показать всю</a>
                <div class="js-editormd-view-container" id="editormd-view-<?= $article->id ?>">
                    <textarea style="display: none"></textarea>
                </div>
            </div>
        </div>
	<? endforeach; ?>
</div>
