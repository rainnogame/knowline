<?php


namespace app\widgets\articles_list;


use yii\base\Widget;

class ArticlesList extends Widget
{
	public $articles;

	public function run()
	{
		return $this->render('index', ['articles' => $this->articles]);
	}

}