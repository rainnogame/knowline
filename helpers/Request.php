<?php


namespace app\helpers;


class Request
{
    public static function isAjax()
    {
        return \Yii::$app->request->isAjax;
    }

    public static function get($value = null)
    {
        return \Yii::$app->request->get($value);
    }

    public static function post($value = null)
    {
        return \Yii::$app->request->post($value);
    }
}