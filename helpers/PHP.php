<?php


namespace app\helpers;


class PHP
{
    /**
     * @deprecated replaced to ?? operator
     * Return $value, if true, default or null otherwise
     * @param mixed $value test value
     * @param mixed $default
     * @return mixed $value, if true, default or null otherwise
     */
    public static function isDef($value, $default = null)
    {
        return $value ?? $default;
    }

    public static function isDefArr($array, $arrayIndex, $defaultValue = null)
    {
        return isset($array[$arrayIndex]) ? $array[$arrayIndex] : $defaultValue;
    }
}