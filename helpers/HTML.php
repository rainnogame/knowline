<?php


namespace app\helpers;


class HTML extends \yii\bootstrap\Html
{
//	public static function bpLinkBtn($content, $url, $type = 'default', $size = 'xs', $additionalClass = '')
//	{
//		return self::a($content, $url, [
//			'class' => ($type == 'simple' ? '' : "btn btn-$type btn-$size") . " $additionalClass",
//		]);
//	}

	public static function typeIcon($icon)
	{
		return $icon ? self::tag('i', '', [
			'class' => 'type-icon fa fa-' . $icon,
		]) : '';
	}
}